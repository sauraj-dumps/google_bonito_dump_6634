## bonito-user 12 SQ1A.220205.002 8010174 release-keys
- Manufacturer: google
- Platform: sdm710
- Codename: bonito
- Brand: google
- Flavor: bonito-user
- Release Version: 12
- Id: SQ1A.220205.002
- Incremental: 8010174
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/bonito/bonito:12/SQ1A.220205.002/8010174:user/release-keys
- OTA version: 
- Branch: bonito-user-12-SQ1A.220205.002-8010174-release-keys
- Repo: google_bonito_dump_6634


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
